### Install and run using docker/docker_compose:
```shell
sysctl -w vm.max_map_count=292144
docker-compose up -d
```


### Insert data:
```shell
curl -X  POST  "localhost:9200/cadastro/pessoas/1" -H 'Content-Type: application/json' -d'{
    "nome": "João Silva",
    "interesses": ["futebol", "música", "literatura"],
    "cidade": "São Paulo",
    "formação": "Letras",
    "estado": "SP",
    "país": "Brasil"
}'

curl -X  POST  "localhost:9200/cadastro/pessoas/2" -H 'Content-Type: application/json' -d'{
    "nome": "Maria Silva",
    "interesses": ["pintura", "literatura", "teatro"],
    "cidade": "Diamantina",
    "formação": "Artes Plásticas",
    "estado": "MG",
    "país": "Brasil"
}'

curl -X  POST  "localhost:9200/cadastro/pessoas/3" -H 'Content-Type: application/json' -d'{
    "nome": "Richard Edward",
    "interesses": ["matemática", "física", "música"],
    "cidade": "Boston",
    "formação": "Física",
    "estado": "MA",
    "país": "Estados Unidos"
}'


curl -X  POST  "localhost:9200/cadastro/pessoas/4" -H 'Content-Type: application/json' -d'{
    "nome": "Patrick von Steppat",
    "interesses": ["computação", "culinária", "cinema"],
    "cidade": "Rio de Janeiro",
    "formação": "Gastronomia",
    "estado": "RJ",
    "país": "Brasil"
}'
```

### Interrogate settings:
```shell
curl -X GET "localhost:9200/cadastro/_settings" | json_reformat 
PUT /catalogo/_settings
```

### Interrogate data:

#### Check if document id 1 exists:
```shell
curl -I "localhost:9200/cadastro/pessoas/1"
```

#### List all documents on "pessoas" instance:
```shell
curl -X GET "localhost:9200/cadastro/pessoas/_search" | json_reformat 
```

#### List all documents that has "futebol":
```shell
curl -X GET "localhost:9200/cadastro/pessoas/_search?q=futebol" | json_reformat 
```

The above query is the same as:
```shell 
curl -X GET "localhost:9200/cadastro/pessoas/_search?q=_all=futebol" | json_reformat 
```

#### List documents based on specific attribute. Example: "estado"
```shell 
curl -X GET "localhost:9200/cadastro/pessoas/_search?q=_estado=MG" | json_reformat 
```
#### List documents and limit the output for specific number of documents. Example: 1
```shell 
curl -X GET "localhost:9200/cadastro/pessoas/_search?q=_estado=MG&size=1" | json_reformat  
```

#### List documents and limit the output for specific number of documents, starting in specific document. Example: 1, starting from 0
```shell 
curl -X GET "localhost:9200/cadastro/pessoas/_search?q=_estado=MG&size=1&from=0" | json_reformat  
```

#### Update data of specific index.  Example: index 1
```shell
curl -X  POST  "localhost:9200/cadastro/pessoas/1/_update" -H 'Content-Type: application/json' -d'{
    "doc": {
        "nome": "João Silva Filho",
        "interesses": ["futebol", "música", "literatura", "teatro"]
    }
}'

curl -X GET "localhost:9200/cadastro/pessoas/1" | json_reformat 
```

#### Get the schema/type of data
```shell
curl -X GET "localhost:9200/cadastro/_mapping" | json_reformat
```

[Other examples of queries](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-multi-match-query.html)

#### Delete document by inex.  Example: index 1
```shell
curl -X DELETE "localhost:9200/cadastro/pessoas/1" 
```





#### Analizer
 [reference](https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-put-mapping.html])

```shell
curl -X DELETE "localhost:9200/publications"
curl -X PUT "localhost:9200/publications?pretty"

curl -X PUT "localhost:9200/publications/_mapping?pretty" -H 'Content-Type: application/json' -d'
{
  "properties": {
    "title":  { "type": "text"},
    "message": {"type": "text", "analyzer": "portuguese"}
  }
}
'


curl -X  POST  "localhost:9200/publications/_doc/" -H 'Content-Type: application/json' -d'{
    "title": "Publicacao 1",
    "message": "Música teste 1"
}'

curl -X  POST  "localhost:9200/publications/_doc/_search?q=music" | json_reformat
```















##### 
```shell
curl -X DELETE "localhost:9200/pessoa_v2"


curl -X PUT "localhost:9200/pessoa_v2?pretty" -H 'Content-Type: application/json' -d'
{}
'
curl -X PUT "localhost:9200/twitter/_mapping/_doc?pretty" -H 'Content-Type: application/json' -d'
{
  "properties": {
    "interesses": {
       "analyzer": "portuguese" 
    }
  }
}
'



curl -X PUT "localhost:9200/cadastro_v2/_mapping?pretty" -H 'Content-Type: application/json' -d'
{
  "properties": {
    "nome": {
      "analyzer": "portuguese"       
    }
  }
}
'


curl -X  POST  "localhost:9200/pessoa_v2/2" -H 'Content-Type: application/json' -d'{
    "nome": "Maria Silva"
}'

curl -X  POST  "localhost:9200/pessoa_v2/2" -H 'Content-Type: application/json' -d'{
    "nome": "Maria Silva",
    "interesses": ["pintura", "literatura", "teatro", "música"],
    "cidade": "Diamantina",
    "formação": "Artes Plásticas",
    "estado": "MG",
    "país": "Brasil"
}'



curl -X GET "localhost:9200/pessoa_v2/_mapping" | json_reformat
curl -X GET "localhost:9200/pessoa_v2/pessoas/_search?q=musica" | json_reformat 
```

